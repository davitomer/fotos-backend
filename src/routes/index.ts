import {Router} from 'express';
const router = Router();
import { createPhoto, getPhotos, getPhoto, deletedPhoto, upDatedPhoto, deleteAllFiles } from '../controllers/photo.controller';
import  multer  from "../libs/multer";
router.route('/photos')
    .get(getPhotos) 
    .post(multer.single('image'), createPhoto)
    .delete(deleteAllFiles)
    
router.route('/photos/:id')
    .get(getPhoto)
    .delete(deletedPhoto)
    .put(upDatedPhoto)
    
router.route('/file')
   
export default router;
