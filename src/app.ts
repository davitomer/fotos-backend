import express from 'express';
import morgan from 'morgan';
const app = express();
import indexRoutes from './routes/index';
import path from 'path';     
import cors from 'cors';


// Creo seccion settings para configurar nuestro proyecto.
app.set('port', process.env.PORT || 4000) // Si existe puerto, tomalo, si no, usa el puerto 4000. Defino variable 'port'

/* Defino otro modulo mas porque mi proyecto voy a tener que recibir archivos json, voy a recibir peticiones 
Entonces quiero ver ESAS PETICIONES POR CONSOLA. Instalo modulo morgan y despues sus tipos de datos porque node 
no los reconove al importarlo. Aqui configuro morgan, lo defino como seccion MIDDLEWARE PORQUE MORGAN ES UN MIDDLEWARE */
app.use(morgan('dev')); // Uso morgan en el entorno de desarrollo. 
app.use(cors());
app.use(express.json());


/* Confugiro rutas. Pero aqui no lo vamos a meter. Creo una carpeta dentro de SRC que llamaré routes y dentro creamos
un archivo llamado index.ts. Dentro importo metodo  Router de express, lo lanzo y lo guardo en una constante (al lanzarlo
nos devuelve un objeto que es un ENRUTADOR, En este objeto podemos guardar nuestras rutas o URLs de nuestro servidor). 
Ese ENRUTADOR DEFINIDO EN EL ARCHIVO INDEX.TS DE LA CARPETA 'routes' LO VOY A USAR AQUÍ.
Una vez creado, aqui importo arriba desde ./routes/index.ts y despues lo uso indicando una url (por ejemplo ./api) 
Una vez importado digo: quiero que utilices en tus rutas: /.api (es decir, cada vez que pidas en la url: 
    http://localhost:4000/api, todo lo que llegue a esa ruta va a ser manejado por el archivo index.ts dentro de la 
    carpeta routes, y cada vez que llegue esto va a ser manejado con el archivo indexRoutes). 
*/

app.use('/api', indexRoutes);

/* Adicionalmente este servidor (o esa API) va a tener que recibir imagenes y en algun lugar lo tendremos que almacenar 
para esto establecemos una carpeta como publica (fuera de SRC una carpeta public --> upload). Para almacenar archivos 
publicos. Lo pongo fuera de src porqie src solo lo usaré en desarrollo, cuando compile el codigo (cuando convierta el
codigo), recordar que src será colocado en una carpeta dist y en esa carpeta dist tambien va a haber otra estructura,
entonces, para no trabajar con eso, para no saber si estoy dentro de src o dist, simplemente voy a colocar la carpeta 
fuera, es decir, ya no importa si genero el codigo o estoy en desarrollo porque al final siempre se va a estar 
guardando dentro de la carpeta upload que hhe creado y asi me evito de saber en que entorno estoy trabajando.

Establezco la carpeta: Vamos a usar la ruta llamada upload, es decir, esta carpeta va a contener las imagenes pero 
las imagenes pueden ser accedidas por el navegador. Para que el navegador pueda accederlas, establecemos la 
configuracion app.use('/upload') .... Entonces le vamos a decir: En la carpeta upload va a poder ser accedida a traves
de la direccion  http://localhost:4000/upload, es decir, cada vez que ponga esa direccion en el navegador, voy a estar
buscando la carpeta upload y los archivos que tiene dentro. Para poder establecer esto, digo express.static(): Le digo
que voy a utilizar una carpeta estática y le voy a decir en donde está esa carpeta: Para esto, uso un modulo de node
llamado path (lo importo --> import path from 'path';) y luego le digo que desde el modulo path voy a usar su metodo
resolve()  
Desde el inicio de mi sistema vas a llegar hasta la carpeta upload y esto es lo que vamos a publicar en el navegador.
Esto es lo que el navegador puede acceder.
*/
app.use('/uploads', express.static(path.resolve('uploads')));



export default app; 