import { Request, Response } from 'express';
import Photo from '../models/Photo';
import path  from 'path';
import fs from 'fs-extra';

export async function getPhotos (req: Request, res: Response): Promise<Response> {
  const photos = await Photo.find();
  return res.json(photos);
}

export async function getPhoto (req: Request, res: Response): Promise<Response> {
    // console.log(req.params.id)
    // const photo = await Photo.findById(req.params.id);
    const { id } = req.params;
    const photo = await Photo.findById(id);
    return res.json(photo)
    // return res.json({});
    
  }


export async function createPhoto (req: Request, res: Response): Promise<Response>{   
    const {title, description} = req.body;
    /*console.log(req.body)
    console.log(req.file.path)*/    
    const newPhoto = {
        title: title,
        description: description,
        imagePath: req.file.path
    }
    const photo =  new Photo(newPhoto);
    // console.log(photo);
    await photo.save(); 
    // Como toma algo de tiempo, defino el metodo como asincrono.
    return res.json({
        message: 'Photo sucessfuly saved',
        photo        
    })
}

export async function deletedPhoto (req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const photo = await Photo.findByIdAndRemove(id)
    if(photo){
      await fs.unlink(path.resolve(photo.imagePath))
    }
    return res.json({
        message: 'Photo removed',
        photo               
    })
  }


  export async function upDatedPhoto (req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { title, description} = req.body;
    const updatedPhoto = await Photo.findByIdAndUpdate(id, {
      title,
      description
    })
    return res.json({
        message: 'Photo sucessfuly updated',
        updatedPhoto               
    })
  }


  export async function deleteAllFiles (req: Request, res: Response): Promise<Response> {  
    
    const photo = await Photo.deleteMany({})
    const directoryUploadsPhotos = 'uploads';
    fs.readdir(directoryUploadsPhotos, (err, files) => {
      if (err) throw err;    
      for (const file of files) {
        fs.unlink(path.join(directoryUploadsPhotos, file), err => {
          if (err) throw err;
        });
      }
    });
    return res.json({
        message: 'All Photos have been removed',
        photo               
    })
  }
  

  