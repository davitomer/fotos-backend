import {connect} from 'mongoose';
export async function startConnection(){
    await connect('mongodb://localhost/photo-gallery-db',{
       useUnifiedTopology: true,
       useNewUrlParser:true
    })
    .then(() => console.log('Database is connected'))
    .catch(errCatch => {
        console.log('DB connection Error:'+ errCatch);
    })       
}
