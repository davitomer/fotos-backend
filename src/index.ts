import app from './app';
import { startConnection } from './database'; // Importo funcion startConnection de database para la conexión
async function main(){
    startConnection();
    await app.listen(app.get('port')); // Este 'port' lo toma del declarado en app.ts: app.set('port' .....
    console.log("Server on port", app.get('port'));
}
main();
